import mongoose from "mongoose";
var Schema = mongoose.Schema;

//Task model
var taskSchema = Schema
(
    {
        label: {
            type: String,
            required: true
        },
        value: {
            type: String,
            required: false
        },
        type: {
            type: String,
            enum: ['String', 'Date', 'Boolean', 'List'],
            required: true
        },
        state: {
            type: String,
            enum: ['success', 'error', 'default', 'warning'],
            required: false
        },
        textAide: {
            type: String,
            required: false
        },
        documents: {
            type: Array,
            required: false
        },
        defaultValueID: {
            type: String,
            required: false
        },
        currentDataSetId: {
            type: String,
            required: false
        },
        parentForm: {
            type: Schema.ObjectId,
            ref: 'Form'
        },
        isPartOfOneTemplate: {
            type: Boolean,
            required: true,
            default: false
        }
    },
    {
        timestamps: true,
        toJSON: { virtuals: true }
    }
);

taskSchema.virtual('childDataSet', {
    ref: 'DataSet',
    localField: 'currentDataSetId',
    foreignField: '_id'
});


module.exports = mongoose.model('Task', taskSchema);
