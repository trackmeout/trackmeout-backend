import mongoose from "mongoose";
var Schema = mongoose.Schema;

//PatientModel
var patientSchema = Schema
(
    {
        lastName: {
            type: String,
            required: true
        },
        firstName: {
            type: String,
            required: true
        },
        birthDate: {
            type: String,
            required: true
        },
        gender: {
            type: Number, // 0 pour homme 1 pour femme
            required: true
        }
    }
    ,
    {
        timestamps: true
    }
);

export default mongoose.model("Patient", patientSchema);
