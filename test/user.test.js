import mongoose from "mongoose" ;
import UserModel from '../models/UserModel';
import chai from 'chai';
import chaiHttp from 'chai-http';
import server from '../index.js';
let should = chai.should();

chai.use(chaiHttp);

describe('User unti test', () => {

  /*
  * Test delete all patients
  */
  describe('Delete users', () => {
      it('it should DELETE all users', (done) => {
        UserModel.remove({}, (err) => {
          if(err === null)
            done(err);
          else
           done();
        });
      });
  });
  /*
  * Test the /POST signup route
  */
  describe('/POST new user', () => {
      it('it should POST new user', (done) => {
        let myVar = {
          user : {
            confirm:"12345678",
            email:"dev@trackmeout.com",
            firstName:"Jon",
            lastName:"Doe",
            password:"12345678",
          }
        }
        chai.request(server)
            .post('/api-dev/users/signup')
            .send(myVar)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.user.should.have.property('email').eq('dev@trackmeout.com');
                res.body.user.should.have.property('firstname').eq("Jon");
                res.body.user.should.have.property('lastname').eq("Doe");
                res.body.user.should.have.property('token');
                done();
            });
      });

  });

  /*
  * Test the /POST login route
  */
  describe('/POST user login', () => {
      it('it should POST log the user', (done) => {
        let myVar = {
          credentials : {
            email:"dev@trackmeout.com",
            password:"12345678",
          }
        }
        chai.request(server)
            .post('/api-dev/users/login')
            .send(myVar)
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
      });

  });

});
