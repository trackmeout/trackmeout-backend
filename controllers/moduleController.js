import express from "express";
import _ from "lodash";
import ModuleModel from '../models/ModuleModel';
import FormModel from '../models/FormModel';
import TaskModel from '../models/TaskModel';
import DataSetModel from '../models/DataSetModel';
import {deleteModule} from '../utils/global';
import ProcessModel from "../models/ProcessModel";
const router = express.Router();

/**
* get all modules
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.get("/", (req, res) => {
    ModuleModel.find({}).then(module => res.json({module}));
});

/**
* get all modules that are templates
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.get("/templates", (req, res) => {
    ModuleModel.find({isTemplate: true})
    .populate({
        path: 'forms',
        populate: {
            path: 'tasks',
            populate: {
                path: 'childDataSet'
            }
        }
    })
    .exec(function (error, modules) {
            res.json({modules});
        }
    );
});

/**
* convert a normal module as a template module
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.post("/convert-as-template", (req, res) => {

    const moduleId = req.body.module.id;
    const newModuleName = req.body.module.name;

    var totalNbrElementsToAdd = 0;
    var totalNbrElementsAdded = 0;

    function sendResponse() {
        if(totalNbrElementsToAdd === totalNbrElementsAdded){
            res.json({success: totalNbrElementsToAdd > 0 });
        }
    }
    ModuleModel
        .findOne({_id: moduleId})
        .populate({
                path: 'forms',
                populate: {
                    path: 'tasks',
                    populate: {
                        path: 'childDataSet'
                    }
                }
        })
        .exec(function (error, oldModule) {
            totalNbrElementsToAdd++;
            const newModule = new ModuleModel({
                name: newModuleName,
                isTemplate: true,
                isPartOfOneTemplate: true,
                isArchived: false,
                parentTemplate: null,
            });
            newModule.save((err, moduleSaved) =>{
                if(err){
                    res.json({success: false});
                    return;
                }
                totalNbrElementsAdded++;
                //Boucle sur formulaires
                oldModule.forms.map((form) =>{

                    totalNbrElementsToAdd++;
                    const newForm =  new FormModel({
                        name: form.name,
                        parentModule: null,
                        isTemplate: false,
                        isPartOfOneTemplate: true,
                        parentTemplate: null,
                        isArchived: false,
                        parentModule: moduleSaved._id
                    });
                    newForm.save((err, formSaved) => {
                        if (err) {
                            res.json({success: false});
                            return;
                        }
                        totalNbrElementsAdded++;
                        form.tasks.map(( task ) => {
                            totalNbrElementsToAdd++;
                            let NewTask;
                            if(task.type === "List"){
                              NewTask =  new TaskModel({
                                  label: task.label,
                                  value: '-',
                                  type: task.type,
                                  isTemplate: false,
                                  isPartOfOneTemplate: task.isTemplate === true,
                                  textAide: task.textAide,
                                  documents:[],
                                  defaultValueID: task.defaultValueID,
                                  currentDataSetId: task.currentDataSetId,
                                  parentForm: formSaved._id,
                              });
                            }
                            else{
                              NewTask =  new TaskModel({
                                  label: task.label,
                                  value: '-',
                                  type: task.type,
                                  isTemplate: false,
                                  isPartOfOneTemplate: task.isTemplate === true,
                                  textAide: task.textAide,
                                  documents:[],
                                  parentForm: formSaved._id,
                                  state: 'default'
                              });
                            }

                            NewTask.save((err, taskSaved) => {
                                if (err) {
                                    res.json({success: false});
                                    return;
                                }
                                else{
                                  totalNbrElementsAdded++;
                                  sendResponse();
                                }
                            });
                            sendResponse();
                        });
                        sendResponse();
                    });
                    sendResponse();
                });
                sendResponse();
            });
        });
});

/**
* get a module template by his ID
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.get("/templates/:id", (req, res) => {
    var id = req.params.id;
    ModuleModel.findOne({_id: id})
        .populate({
            path: 'forms',
            populate: {
                path: 'tasks',
                populate: {
                    path: 'childDataSet'
                }
            }
        })
        .exec(function (error, modules) {
                res.json({modules});
            }
        );
});


/**
* get all module templates by his parent processus ID
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.get("/templates/parent/:id", (req, res) => {
    var id = req.params.id;
    ModuleModel.find({parentProcess: id})
        .populate({
            path: 'forms',
            populate: {
                path: 'tasks',
                populate: {
                    path: 'childDataSet'
                }
            }
        })
        .exec(function (error, modules) {
                res.json({modules});
            }
        );
});

/**
* get module by his ID
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.get("/:id", (req, res) => {
    var id = req.params.id;
    ModuleModel.find({"_id": id}).then(module => res.json({module}));
});

/**
* create new module
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.post("/", (req, res) => {
    ModuleModel.create(req.body.module)
        .then(module => res.json({module}))
        .catch(err => res.status(400).json({errors: (err.errors)}));
});

/**
* create a form template in a module
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.post("/add-form-from-template", (req, res) => {

    const targetModuleId = req.body.data.moduleId;
    const templateFormIdToAdd = req.body.data.formId;

	var totalNbrElementsToAdd = 0;
	var totalNbrElementsAdded = 0;

	function sendResponse() {
		if(totalNbrElementsToAdd == totalNbrElementsAdded){
			res.json({success: totalNbrElementsToAdd > 0 });
		}
	}

    FormModel.findOne({_id: templateFormIdToAdd})
        .populate({
            path: 'tasks',
            populate: {
                path: 'childDataSet'
            }
        })
        .exec(function (error, modelForm) {
			totalNbrElementsToAdd++;
            const newForm =  new FormModel({
                name: modelForm.name,
                parentModule: targetModuleId,
                isTemplate: false,
                isPartOfOneTemplate: modelForm.isTemplate === true,
                parentTemplate: modelForm._id,
            });
            newForm.save((err, formSaved) => {

                if(err) {
                	res.json({success: false});
                	return;
                }
				totalNbrElementsAdded++;
                modelForm.tasks.map(( task ) => {
				  totalNbrElementsToAdd++;
                  let NewTask;
                  if(task.type === "List"){
                    NewTask =  new TaskModel({
                        label: task.label,
                        value: '-',
                        type: task.type,
                        isTemplate: false,
                        isPartOfOneTemplate: task.isTemplate === true,
                        textAide: task.textAide,
                        documents:[],
                        defaultValueID: task.defaultValueID,
                        currentDataSetId: task.currentDataSetId,
                        parentForm: formSaved._id,
                    });
                  }
                  else{
                    NewTask =  new TaskModel({
                        label: task.label,
                        value: '-',
                        type: task.type,
                        isTemplate: false,
                        isPartOfOneTemplate: task.isTemplate === true,
                        textAide: task.textAide,
                        documents:[],
                        parentForm: formSaved._id,
                        state: 'default'
                    });
                  }

                  NewTask.save((err, taskSaved) => {
                      if (err) {
                          res.json({success: false});
                          return;
                      }
                      else {
						  totalNbrElementsAdded++;
						  sendResponse();
					  }
				  });
					sendResponse();
				});
				sendResponse();
			});
		});
});

/**
* add many forms from template to a module
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.post("/add-forms-from-template", (req, res) => {

    const dataToAdd = req.body.data;
    const totalNbrElementsToAdd = dataToAdd.length;
    var totalNbrElementsAdded = 0;

    function sendResponse() {
        if(totalNbrElementsToAdd == totalNbrElementsAdded){
            res.json({success: totalNbrElementsToAdd > 0});
        }
    }

    _.each(dataToAdd, function (currentData) {

        const targetModuleId = currentData.moduleId;
        const templateFormIdToAdd = currentData.formId;

        FormModel.findOne({_id: templateFormIdToAdd})
            .populate({
                path: 'tasks',
                populate: {
                    path: 'childDataSet'
                }
            })
            .exec(function (error, modelForm) {

                const newForm =  new FormModel({
                    name: modelForm.name,
                    parentModule: targetModuleId,
                    isTemplate: false,
                    isPartOfOneTemplate: false,
                    parentTemplate: modelForm._id,
                });

                newForm.save((err, formSaved) => {

                    if(err){ res.json({success: false}); }

                    modelForm.tasks.map(( task ) => {
                      let NewTask;
                      if(task.type === "List"){
                        NewTask =  new TaskModel({
                            label: task.label,
                            value: '-',
                            type: task.type,
                            isTemplate: false,
                            isPartOfOneTemplate: task.isTemplate === true,
                            textAide: task.textAide,
                            documents:[],
                            defaultValueID: task.defaultValueID,
                            currentDataSetId: task.currentDataSetId,
                            parentForm: formSaved._id,
                        });
                      }
                      else{
                        NewTask =  new TaskModel({
                            label: task.label,
                            value: '-',
                            type: task.type,
                            isTemplate: false,
                            isPartOfOneTemplate: task.isTemplate === true,
                            textAide: task.textAide,
                            documents:[],
                            parentForm: formSaved._id,
                            state: 'default'
                        });
                      }

                      NewTask.save((err, taskSaved) => {
                          if (err) {
                              res.json({success: false});
                              return;
                          }
                          totalNbrElementsAdded++;
                          sendResponse();
                      });
                    });

                    totalNbrElementsAdded++;
                    sendResponse();
                })
            });
    });
    sendResponse();

});

/**
* add many module to a processus from module templates
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.post("/add-modulesToProcess-from-template/", (req, res) => {
	const modules = req.body.modules;
	const processID = req.body.processID;
	var totalNbrElementsToAdd = 0;
	var totalNbrElementsAdded = 0;

	function sendResponse() {
		if (totalNbrElementsToAdd === totalNbrElementsAdded) {
			res.json({success: totalNbrElementsToAdd > 0});
		}
	}

	modules.map((module) => {
		//Creatiom modules
		totalNbrElementsToAdd++;
		const newModule = new ModuleModel({
			name: module.name,
			roles: [],
			parentProcess: processID,
			isTemplate: false,
			isPartOfOneTemplate: module.isTemplate === true,
			parentTemplate: module._id,
		});
		newModule.save((err, moduleSaved) => {
			if (err) {
				res.json({success: false});
				return;
			}
			else {
				totalNbrElementsAdded++;
				module.forms.map((form) => {
					totalNbrElementsToAdd++;
					const newForm = new FormModel({
						name: form.name,
						parentModule: moduleSaved._id,
						isTemplate: false,
						isPartOfOneTemplate: form.isTemplate === true,
					});
					newForm.save((err, formSaved) => {
						if (err) {
							res.json({success: false});
							return;
						}
						else {
							totalNbrElementsAdded++;
							form.tasks.map((task) => {
								totalNbrElementsToAdd++;
								let newTask;
								if (task.type === "List") {
									newTask = new TaskModel({
										label: task.label,
										value: '-',
										type: task.type,
										isTemplate: false,
										isPartOfOneTemplate: task.isTemplate === true,
										textAide: task.textAide,
										documents: [],
										defaultValueID: task.defaultValueID,
										currentDataSetId: task.currentDataSetId,
										parentForm: formSaved._id,
									});
								}
								else {
									newTask = new TaskModel({
										label: task.label,
										value: '-',
										type: task.type,
										isTemplate: false,
										isPartOfOneTemplate: task.isTemplate === true,
										textAide: task.textAide,
										documents: [],
										parentForm: formSaved._id,
										state: 'default'
									});
								}
								newTask.save((err, taskSaved) => {
									if (err) {
										res.json({success: false});
										return;
									} else {
										totalNbrElementsAdded++;
										sendResponse();
									}
								});
								sendResponse();
							});
							sendResponse();
						}
					});
					sendResponse();
				});
				sendResponse();
			}
		});
		sendResponse();
	});
	sendResponse();
});

/**
* update a module name
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.put("/", (req, res) => {
    var query = {_id: req.body.module.id};
    ModuleModel.update(query, {name: req.body.module.name}, function (err, raw) {
        if (err) {
            res.send(err);
        }
        res.send(raw);
    });
});

/**
* delete a module
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.delete("/", (req, res) => {
    var moduleId = req.body.module._id;
    deleteModule(moduleId).then(module => res.json({module}));

});

export default router;
