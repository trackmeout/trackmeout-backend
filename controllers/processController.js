import express from "express";
import ProcessModel from '../models/ProcessModel';
import ModuleModel from '../models/ModuleModel';
import FormModel from '../models/FormModel';
import TaskModel from '../models/TaskModel';
const router = express.Router();

/**
* get all non-archived processus
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.get("/", (req, res) => {
    ProcessModel.find({isArchived: false}).then(processes => res.json({processes}));
});

/**
* get all template processes
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
/**
* convert processus normal to a template processus
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.post("/convert-as-template", (req, res) => {

    const processId = req.body.process.id;
    const newProcessName = req.body.process.name;

    var totalNbrElementsToAdd = 0;
    var totalNbrElementsAdded = 0;

    function sendResponse() {
        if(totalNbrElementsToAdd === totalNbrElementsAdded){
            res.json({success: totalNbrElementsToAdd > 0 });
        }
    }

    ProcessModel.findOne({_id: processId})
        .populate({
            path: 'modules',
            populate: {
                path: 'forms',
                populate: {
                    path: 'tasks',
                    populate: {
                        path: 'childDataSet'
                    }
                }
            }
        })
        .exec(function (error, oldProcess) {
            totalNbrElementsToAdd++;
            const newProcess =  new ProcessModel({
                name: newProcessName,
                parentModule: null,
                isTemplate: true,
                isPartOfOneTemplate: true,
                parentTemplate: null,
                isArchived: false
            });

            newProcess.save((err, processSaved) => {
                if(err){
                    res.json({success: false});
                    return;
                }
                totalNbrElementsAdded++;
                //Boulce sur modules
                oldProcess.modules.map(( oldModule ) => {
                    totalNbrElementsToAdd++;
                    const newModule =  new ModuleModel({
                        name: oldModule.name,
                        parentModule: null,
                        isTemplate: false,
                        isPartOfOneTemplate: true,
                        parentTemplate: null,
                        isArchived: false,
                        parentProcess: processSaved._id
                    });

                    newModule.save((err, moduleSaved) => {
                        if (err) {
                            res.json({success: false});
                            return;
                        }
                        totalNbrElementsAdded++;
                        oldModule.forms.map(( oldForm ) => {
                            totalNbrElementsToAdd++;

                            const newForm =  new FormModel({
                                name: oldForm.name,
                                parentModule: null,
                                isTemplate: false,
                                isPartOfOneTemplate: true,
                                parentTemplate: null,
                                isArchived: false,
                                parentModule: moduleSaved._id
                            });

                            newForm.save((err, formSaved) => {
                                if (err) {
                                    res.json({success: false});
                                    return;
                                }
                                totalNbrElementsAdded++;
                                oldForm.tasks.map(( task ) => {
                                    totalNbrElementsToAdd++;
                                    let NewTask;
                                    if(task.type === "List"){
                                      NewTask =  new TaskModel({
                                          label: task.label,
                                          value: '-',
                                          type: task.type,
                                          isTemplate: false,
                                          isPartOfOneTemplate: task.isTemplate === true,
                                          textAide: task.textAide,
                                          documents:[],
                                          defaultValueID: task.defaultValueID,
                                          currentDataSetId: task.currentDataSetId,
                                          parentForm: formSaved._id,
                                      });
                                    }
                                    else{
                                      NewTask =  new TaskModel({
                                          label: task.label,
                                          value: '-',
                                          type: task.type,
                                          isTemplate: false,
                                          isPartOfOneTemplate: task.isTemplate === true,
                                          textAide: task.textAide,
                                          documents:[],
                                          parentForm: formSaved._id,
                                          state: 'default'
                                      });
                                    }

                                    NewTask.save((err, taskSaved) => {
                                        if (err) {
                                            res.json({success: false});
                                            return;
                                        }
                                        else{
                                          totalNbrElementsAdded++;
                                          sendResponse();
                                        }
                                    });
                                    sendResponse();
                                });
                                sendResponse();
                            });
                            sendResponse();
                        });
                        sendResponse();
                    });
                    sendResponse();
                });
                sendResponse();
            });
        });
});


/**
* get processus by patient ID
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.get("/owner/:id", (req, res) => {
    var id = req.params.id;
    ProcessModel
        .find({"owner": id, isArchived: false})
        .populate({
            path: 'modules',
            populate: {
                path: 'forms',
                populate: {
                    path: 'tasks',
                    populate: {
                      path: 'childDataSet'
                    }
                }
            }
        })
        .exec(function (error, processes) {
                res.json({processes});
            }
        );
});

/**
* get processus that are templates and not archived
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.get("/are-templates/", (req, res) => {
    ProcessModel
        .find({"isTemplate": true, "isArchived": false})
        .exec(function (error, processes) {
                res.json({processes});
            }
        );
});

/**
* create a new process
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.post("/", (req, res) => {
    ProcessModel.create(req.body.process)
        .then(process => res.json({process}))
        .catch(err => res.status(400).json({errors: parseErrors(err.errors)}));
});

/**
* create a new process template from normal process
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.post("/add-processes-from-template/", (req, res) => {
    const processes = req.body.processes;
    const patientID = req.body.patientID;

    var totalNbrElementsToAdd = 0;
    var totalNbrElementsAdded = 0;

    function sendResponse() {
        if(totalNbrElementsToAdd === totalNbrElementsAdded){
            res.json({success: totalNbrElementsToAdd > 0 });
        }
    }

    processes.map(( process ) => {
      ProcessModel
        .findOne({"_id": process, isArchived: false})
        .populate({
            path: 'modules',
            populate: {
                path: 'forms',
                populate: {
                    path: 'tasks',
                    populate: {
                      path: 'childDataSet'
                    }
                }
            }
        })
        .exec(function (error, currentProcces) {
          totalNbrElementsToAdd++;
          const newProcess =  new ProcessModel({
              name: currentProcces.name,
              isArchived: false,
              roles: [],
              owner: patientID,
              isTemplate: false,
              isPartOfOneTemplate: currentProcces.isTemplate === true
          });
          newProcess.save((err, processSaved) => {
              if(err){
                res.json({success: false});
                return;
              }
              else{
                totalNbrElementsAdded++;
                currentProcces.modules.map(( modules ) => {
                    //Creatiom modules
                    totalNbrElementsToAdd++;
                    const newModule =  new ModuleModel({
                        name: modules.name,
                        roles: [],
                        parentProcess: processSaved._id,
                        isTemplate: false,
                        isPartOfOneTemplate: modules.isTemplate === true,
                        parentTemplate: currentProcces._id,
                    });
                    newModule.save((err, moduleSaved) => {
                      if(err){
                        res.json({success: false});
                        return;
                      }
                      else{
                        totalNbrElementsAdded++;
                        modules.forms.map(( form ) => {
                          totalNbrElementsToAdd++;
                          const newForm =  new FormModel({
                              name: form.name,
                              parentModule: moduleSaved._id,
                              isTemplate: false,
                              isPartOfOneTemplate: form.isTemplate === true,
                          });
                          newForm.save((err, formSaved) => {
                            if(err){
                              res.json({success: false});
                              return;
                            }
                            else{
                              totalNbrElementsAdded++;
                              form.tasks.map(( task ) => {
                                totalNbrElementsToAdd++;
                                let newTask;
                                if(task.type === "List"){
                                  newTask =  new TaskModel({
                                      label: task.label,
                                      value: '-',
                                      type: task.type,
                                      isTemplate: false,
                                      isPartOfOneTemplate: task.isTemplate === true,
                                      textAide: task.textAide,
                                      documents:[],
                                      defaultValueID: task.defaultValueID,
                                      currentDataSetId: task.currentDataSetId,
                                      parentForm: formSaved._id,
                                  });
                                }
                                else{
                                  newTask =  new TaskModel({
                                      label: task.label,
                                      value: '-',
                                      type: task.type,
                                      isTemplate: false,
                                      isPartOfOneTemplate: task.isTemplate === true,
                                      textAide: task.textAide,
                                      documents:[],
                                      parentForm: formSaved._id,
                                      state: 'default'
                                  });
                                }
                                newTask.save((err, taskSaved) => {
                                  if(err){
                                    res.json({success: false});
                                    return;
                                  }else{
                                    totalNbrElementsAdded++;
                                    sendResponse();
                                  }
                                });
                                sendResponse();
                              });
                              sendResponse();
                            }
                          });
                          sendResponse();
                        });
                        sendResponse();
                      }
                    });
                    sendResponse();
                });
                sendResponse();
              }
          });
          sendResponse();
        });
    });
});

/**
* edit a process name
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.put("/", (req, res) => {
  var query = { _id : req.body.process.id };
  ProcessModel.update(query, { name : req.body.process.name }, function(err, raw) {
    if (err) {
      res.send(err);
    }
    res.send(raw);
  });
});

/**
* create a new module template and add it to process template
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.post("/add-module-temp-to-proc-temp", (req, res) => {

    let processId = req.body.id;
    let module = req.body.data;
    const newModule =  new ModuleModel({
        name: module.name,
        roles: [],
        parentProcess: processId,
        isTemplate: true,
        isPartOfOneTemplate: module.isTemplate === true,
        parentTemplate: processId,
    });
    newModule.save((err, moduleSaved) => {
        if (err) {
            res.json({success: false});
        }
        else{
          res.json({success: true});
        }
    });


});

/**
* archive a processus
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.put("/archive/", (req, res) => {
    ProcessModel.update({ _id : req.body.processId }, { isArchived : true }, function(err, raw) {
        if (err) {
            res.send(err);
        }
        res.send(raw);
    });
});

export default router;
