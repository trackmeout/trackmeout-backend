import mongoose from "mongoose";
var Schema = mongoose.Schema;

//Form model
var formSchema = Schema
(
    {
        name: {
            type: String,
            required: true
        },
        parentModule: {
            type: Schema.ObjectId,
            ref: 'Module'
        },
        roles: [
            {
                type: Schema.ObjectId,
                ref: 'Role'
            }
        ],
        isTemplate: {
            type: Boolean,
            required: true,
            default: false
        },
        isPartOfOneTemplate: {
            type: Boolean,
            required: true,
            default: false
        },
        parentTemplate: {
            type: Schema.ObjectId,
            ref: 'Form'
        }
    },
    {
        timestamps: true,
        toJSON: {virtuals: true}
    }
);
formSchema.virtual('tasks', {
    ref: 'Task',
    localField: '_id',
    foreignField: 'parentForm',
    justOne: false
});

module.exports = mongoose.model('Form', formSchema);
