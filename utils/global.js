import TaskModel from '../models/TaskModel';
import FormModel from '../models/FormModel';
import ModuleModel from '../models/ModuleModel';


//Global function used in all the backend


/**
 * delete a task to oncascade delete
 *
 * @param {type} idTask Description
 *
 * @return {type} Description
 */
export function deleteTask(idTask){
  return TaskModel.remove({"_id":idTask})
    .then(task => task)
}

/**
* Delete a form -> oncascade
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
export function deleteForm(idForm){
  return FormModel.remove({"_id":idForm})
    .then(form => {
      TaskModel.remove({"parentForm":idForm}).then(task => task);
      return form;
    });
}


/**
 * deleteModule - delete a module -> on cascade
 *
 * @param {type} idModule Description
 *
 * @return {type} Description
 */
export function deleteModule(idModule){
   FormModel.find({"parentModule": idModule}).then(( forms ) => {
      for(var i = 0; i < forms.length; i++){
        deleteForm(forms[i]._id);
      }
    });
    return ModuleModel.remove({"_id":idModule}).then(module =>  {return module});
}


/**
 * populateTaskInForms - populate exemple tasks in a form
 *
 * @param {type} res Description
 *
 * @return {type} Description
 */

export function createTask(res, parent, type, name, value){
  var newTask = new TaskModel({
      label: name,
      value: value,
      type: type,
      state: "default",
      parentForm: parent
  });

  newTask.save(function (error) {
      if (error) {
          console.error(error);
          res.json({"log": "Task could not be saved"});
          return;
      }
  });
}
