import mongoose from "mongoose" ;
import ModuleModel from '../models/ModuleModel';
import FormModel from '../models/FormModel';
import ProcessModel from '../models/ProcessModel';
import chai from 'chai';
import chaiHttp from 'chai-http';
import server from '../index.js';
let should = chai.should();

chai.use(chaiHttp);

describe('Module unit test', () => {

  let id = 0;
  let idTemplate = 0;
  let parentProcess = 0;
  /*
  * Test delete all modules
  */
  describe('Delete modules', () => {
      it('it should DELETE all modules', (done) => {
        ModuleModel.remove({}, (err) => {
          if(err === null)
            done(err);
          else
           done();
        });
      });
  });

  /*
  * Test the /POST route
  */
  describe('/POST new module ', () => {
      it('it should POST new module ', (done) => {
        let myVar = {
          module : {
            name:"TestModule",
            parentProcess:"5ae31df4e2b1fc26a8a18d2b"
          }
        }
        chai.request(server)
            .post('/api-dev/modules/')
            .send(myVar)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.module.should.have.property('isPartOfOneTemplate').eq(false);
                res.body.module.should.have.property('_id');
                res.body.module.should.have.property('isTemplate').eq(false);
                res.body.module.should.have.property('parentProcess').eq('5ae31df4e2b1fc26a8a18d2b');
                res.body.module.should.have.property('name').eq('TestModule');
                res.body.module.should.have.property('forms').eq(null);
                parentProcess = res.body.module.parentProcess;
                id = res.body.module._id;
                done();
            });
      });

  });

  /*
  * Test the /GET route
  */
  describe('/GET modules', () => {
      it('it should GET all modules', (done) => {
        chai.request(server)
            .get('/api-dev/modules/')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.module.length.should.be.eql(1);
                res.body.module[0].should.have.property('isPartOfOneTemplate').eq(false);
                res.body.module[0].should.have.property('_id');
                res.body.module[0].should.have.property('isTemplate').eq(false);
                res.body.module[0].should.have.property('parentProcess').eq('5ae31df4e2b1fc26a8a18d2b');
                res.body.module[0].should.have.property('name').eq('TestModule');
                res.body.module[0].should.have.property('forms').eq(null);
                id = res.body.module[0]._id;
                done();
            });
      });
  });

  /*
  * Test the /GET route
  */
  describe('/GET modules by id', () => {
      it('it should GET module by id', (done) => {
        chai.request(server)
            .get('/api-dev/modules/'+id)
            .send({"params" : {"id":id}})
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.module.length.should.be.eql(1);
                res.body.module[0].should.have.property('isPartOfOneTemplate').eq(false);
                res.body.module[0].should.have.property('_id');
                res.body.module[0].should.have.property('isTemplate').eq(false);
                res.body.module[0].should.have.property('parentProcess').eq('5ae31df4e2b1fc26a8a18d2b');
                res.body.module[0].should.have.property('name').eq('TestModule');
                done();
            });
      });
  });

  /*
  * Test the /PUT route
  */
  describe('/PUT modules by id', () => {
      it('it should modify module by id', (done) => {
        let myVar = {
          module : {
            "name":"moduleModified",
            "parentProcess":"5ae31dfee2b1fc26a8a18d2c",
            "id":id
          }
        }
        chai.request(server)
            .put('/api-dev/modules/')
            .send(myVar)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.should.have.property('nModified').eq(1);
                done();
            });
      });
  });

  /*
  * Test the /POST route
  */
  describe('/POST new module ', () => {
      it('it should POST new module as template ', (done) => {
        let myVar = {
          module : {
            name:"testModuleAsTemplate",
            parentProcess:"5ae31df4e2b1fc26a8a18d2b",
            isTemplate:true,
          }
        }
        chai.request(server)
            .post('/api-dev/modules/')
            .send(myVar)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.module.should.have.property('isPartOfOneTemplate').eq(false);
                res.body.module.should.have.property('_id');
                res.body.module.should.have.property('isTemplate').eq(true);
                res.body.module.should.have.property('parentProcess').eq('5ae31df4e2b1fc26a8a18d2b');
                res.body.module.should.have.property('name').eq('testModuleAsTemplate');
                res.body.module.should.have.property('forms').eq(null);
                idTemplate = res.body.module._id;

                done();
            });
      });

  });

  /*
  * Test the /GET route
  */
  describe('/GET modules as template', () => {
      it('it should GET modules as template', (done) => {
        chai.request(server)
            .get('/api-dev/modules/templates/')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.modules.length.should.be.eql(1);
                res.body.modules[0].should.have.property('isPartOfOneTemplate').eq(false);
                res.body.modules[0].should.have.property('_id');
                res.body.modules[0].should.have.property('isTemplate').eq(true);
                res.body.modules[0].should.have.property('parentProcess').eq('5ae31df4e2b1fc26a8a18d2b');
                res.body.modules[0].should.have.property('name').eq('testModuleAsTemplate');
                done();
            });
      });
  });

  /*
  * Test the /GET route
  */
  describe('/GET modules as template by id', () => {
      it('it should GET module as template by id', (done) => {
        chai.request(server)
            .get('/api-dev/modules/templates/'+idTemplate)
            .send({"params" : idTemplate})
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.modules.should.have.property('isPartOfOneTemplate').eq(false);
                res.body.modules.should.have.property('_id');
                res.body.modules.should.have.property('isTemplate').eq(true);
                res.body.modules.should.have.property('parentProcess').eq('5ae31df4e2b1fc26a8a18d2b');
                res.body.modules.should.have.property('name').eq('testModuleAsTemplate');
                done();
            });
      });
  });

  /*
  * Test the /POST route
  */
  describe('/POST convert module  ', () => {
      it('it should convert module as template ', (done) => {
        let module = new ModuleModel({
            "id": id,
            name:"moduleConvertedAsTemplate",
            forms : []
        });
        module.save((err, module) => {
          let form = new FormModel({
            "isTemplate": false,
            "isPartOfOneTemplate": false,
            "_id": "5ac527c52c74a10528fd8217",
            "name": "Formulaire 3",
            "parentModule": module.id,
          });
          form.save((err, form) => {
            chai.request(server)
                .post('/api-dev/modules/convert-as-template/')
                .send({"module" : module})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('Object');
                    res.body.should.have.property('success').eq(true);
                    done();
                });
            });
          });
      });
  });

  /*
  * Test the /GET route
  */
  describe('/GET modules as template by parent processus', () => {
      it('it should GET modules by parent processus', (done) => {
        chai.request(server)
            .get('/api-dev/modules/templates/parent/'+parentProcess)
            .send({"params" : {"id":parentProcess}})
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.modules.length.should.be.eql(2);
                res.body.modules[0].should.have.property('isPartOfOneTemplate').eq(false);
                res.body.modules[0].should.have.property('_id');
                res.body.modules[0].should.have.property('isTemplate').eq(false);
                res.body.modules[0].should.have.property('parentProcess').eq('5ae31df4e2b1fc26a8a18d2b');
                res.body.modules[0].should.have.property('name').eq('moduleModified');
                done();
            });
      });
  });

  /*
  * Test the /POST route
  */
  describe('/POST add form to module  ', () => {
      it('it should add form to module template ', (done) => {
        chai.request(server)
            .post('/api-dev/modules/add-form-from-template/')
            .send({data:{moduleId : idTemplate, formId : "5ac527c52c74a10528fd8217"}})
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.should.have.property('success').eq(true);
                done();
            });
      });
  });

  /*
  * Test the /POST route
  */
  describe('/POST add forms to module  ', () => {
      it('it should add forms to module template ', (done) => {
        chai.request(server)
            .post('/api-dev/modules/add-forms-from-template/')
            .send({data:[{moduleId : idTemplate, formId : "5ac527c52c74a10528fd8217"},{moduleId : idTemplate, formId : "5ac527c52c74a10528fd8217"}]})
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.should.have.property('success').eq(true);
                done();
            });
      });
  });

  /*
  * Test the /POST route
  */
  describe('/POST add module template to process  ', () => {
      it('it should add module template to process ', (done) => {
        let process = new ProcessModel({
          name:"TestProc",
          owner:"5ae2f80a69d31d0040e70941"
        });
        process.save((err, proc) => {
          let module = new ModuleModel({
              "id": id,
              name:"moduleConvertedAsTemplate",
              forms : []
          });
          module.save((err, modules) => {
            chai.request(server)
                .post('/api-dev/modules/add-modulesToProcess-from-template/')
                .send({"modules":[modules],"processID":proc._id})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('Object');
                    res.body.should.have.property('success').eq(true);
                    done();
                });
           });
        });
      });
  });


  /*
 * Test the /DELETE/:id route
 */
 describe('/DELETE/:id module', () => {
     it('it should DELETE a module by the given id', (done) => {
         chai.request(server)
         .delete('/api-dev/modules/')
         .send({ "module" : {_id : idTemplate}})
         .end((err, res) => {
             res.should.have.status(200);
             res.body.should.be.a('object')
             res.body.module.should.have.property('ok').eq(1);
           done();
         });
     });
 });

});
