import mongoose from "mongoose" ;
import TaskModel from '../models/TaskModel';
import chai from 'chai';
import chaiHttp from 'chai-http';
import server from '../index.js';
let should = chai.should();

chai.use(chaiHttp);

describe('Task unit test', () => {

  /*
  * Test delete all tasks
  */
  describe('Delete tasks', () => {
      it('it should DELETE all tasks', (done) => {
        TaskModel.remove({}, (err) => {
          if(err === null)
            done(err);
          else
           done();
        });
      });
  });

  /*
  * Test the /POST route
  */
  describe('/POST new task STRING', () => {
      it('it should POST new task type STRING', (done) => {
        let myVar = {
          task : {
            isPartOfOneTemplate:false,
            label:"Test",
            parentForm:"5ae0bc70906f292440482bae",
            state:"default",
            textAide:"Info",
            type:"String",
            value:"-"
          }
        }
        chai.request(server)
            .post('/api-dev/tasks/')
            .send(myVar)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.task.should.have.property('documents');
                res.body.task.should.have.property('isPartOfOneTemplate').eq(false);
                res.body.task.should.have.property('_id');
                res.body.task.should.have.property('label').eq('Test');
                res.body.task.should.have.property('parentForm').eq('5ae0bc70906f292440482bae');
                res.body.task.should.have.property('state').eq('default');
                res.body.task.should.have.property('textAide').eq('Info');
                res.body.task.should.have.property('type').eq('String');
                res.body.task.should.have.property('value').eq('-');
                res.body.task.should.have.property('childDataSet').eq(null);
                done();
            });
      });

  });

  /*
  * Test the /POST route
  */
  describe('/POST new task DATE', () => {
      it('it should POST new task type DATE', (done) => {
        let myVar = {
          task : {
            label:"Date",
            parentForm:"5ae0bc70906f292440482bae",
            state:"default",
            textAide:"",
            type:"Date",
            value:"-"
          }
        }
        chai.request(server)
            .post('/api-dev/tasks/')
            .send(myVar)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.task.should.have.property('documents');
                res.body.task.should.have.property('isPartOfOneTemplate').eq(false);
                res.body.task.should.have.property('_id');
                res.body.task.should.have.property('label').eq('Date');
                res.body.task.should.have.property('parentForm').eq('5ae0bc70906f292440482bae');
                res.body.task.should.have.property('state').eq('default');
                res.body.task.should.have.property('textAide').eq('');
                res.body.task.should.have.property('type').eq('Date');
                res.body.task.should.have.property('value').eq('-');
                res.body.task.should.have.property('childDataSet').eq(null);
                done();
            });
      });

  });

  /*
  * Test the /POST route
  */
  describe('/POST new task LIST', () => {
      it('it should POST new task type LIST', (done) => {
        let myVar = {
          task : {
            childDataSet:{
              dataset: [{id: 1524680126541, title: "Pending", status: "warning"},
                        {id: 1524680126542, title: "Refused", status: "error"},
                        {id: 1524680126543, title: "Accepted", status: "success"},
                        {id: 1524680126544, title: "To start", status: "default"}]},
            defaultValueID:1524680126544,
            isPartOfOneTemplate:false,
            label:"Dataset",
            parentForm:"5ae0bc70906f292440482bae",
            state:"default",
            textAide:"MyInfo",
            type:"List",
          }
        }
        chai.request(server)
            .post('/api-dev/tasks/')
            .send(myVar)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.task.should.have.property('documents');
                res.body.task.should.have.property('isPartOfOneTemplate').eq(false);
                res.body.task.should.have.property('_id');
                res.body.task.should.have.property('label').eq('Dataset');
                res.body.task.should.have.property('parentForm').eq('5ae0bc70906f292440482bae');
                res.body.task.should.have.property('state').eq('default');
                res.body.task.should.have.property('textAide').eq('MyInfo');
                res.body.task.should.have.property('type').eq('List');
                res.body.task.should.have.property('childDataSet');
                done();
            });
      });

  });

  /*
  * Test the /GET route
  */
  describe('/GET tasks', () => {
      it('it should GET all tasks', (done) => {
        chai.request(server)
            .get('/api-dev/tasks/')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.task.length.should.be.eql(3);
                res.body.task[0].should.have.property('documents');
                res.body.task[0].should.have.property('isPartOfOneTemplate').eq(false);
                res.body.task[0].should.have.property('_id');
                res.body.task[0].should.have.property('label').eq('Test');
                res.body.task[0].should.have.property('parentForm').eq('5ae0bc70906f292440482bae');
                res.body.task[0].should.have.property('state').eq('default');
                res.body.task[0].should.have.property('textAide').eq('Info');
                res.body.task[0].should.have.property('type').eq('String');
                res.body.task[0].should.have.property('value').eq('-');
                res.body.task[0].should.have.property('childDataSet').eq(null);
                done();
            });
      });
  });

    /*
   * Test the /GET/:id route
   */
   describe('/GET/:id task', () => {
       it('it should GET a task by the given id', (done) => {
         let task = new TaskModel({ isPartOfOneTemplate:false, label:"TestFind", parentForm:"5ae0bc70906f292440482bae", state:"default", textAide:"Info", type:"String", value:"-" });
         task.save((err, task) => {
             chai.request(server)
             .get('/api-dev/tasks/' + task._id)
             .send(task)
             .end((err, res) => {
                 res.should.have.status(200);
                 res.body.should.be.a('object');
                 res.body.task[0].should.have.property('documents');
                 res.body.task[0].should.have.property('isPartOfOneTemplate').eq(false);
                 res.body.task[0].should.have.property('_id');
                 res.body.task[0].should.have.property('label').eq('TestFind');
                 res.body.task[0].should.have.property('parentForm').eq('5ae0bc70906f292440482bae');
                 res.body.task[0].should.have.property('state').eq('default');
                 res.body.task[0].should.have.property('textAide').eq('Info');
                 res.body.task[0].should.have.property('type').eq('String');
                 res.body.task[0].should.have.property('value').eq('-');
                 res.body.task[0].should.have.property('childDataSet').eq(null);
               done();
             });
         });

       });
   });

   /*
  * Test the /PUT route
  */
   describe('/PUT update task', () => {
      it('it should UPDATE a task given the id type STRING', (done) => {
        let task = new TaskModel({ isPartOfOneTemplate:false, label:"TestFind", parentForm:"5ae0bc70906f292440482bae", state:"default", textAide:"Info", type:"String", value:"-" });
        task.save((err, task) => {
                chai.request(server)
                .put('/api-dev/tasks/')
                .send({ "task" : {_id: task._id, isPartOfOneTemplate:false, label:"TestFindModified", parentForm:"5ae0bc70906f292440482bae", state:"default", textAide:"Info", type:"String", value:"-" }})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('nModified').eq(1);
                  done();
                });
          });
      });
   });

   /*
  * Test the /PUT route
  */
   describe('/PUT update task', () => {
      it('it should UPDATE a task given the id type DATE', (done) => {
        let task = new TaskModel({ isPartOfOneTemplate:false, label:"TestFind", parentForm:"5ae0bc70906f292440482bae", state:"default", textAide:"Info", type:"Date", value:"-" });
        task.save((err, task) => {
                chai.request(server)
                .put('/api-dev/tasks/')
                .send({ "task" : {_id: task._id, isPartOfOneTemplate:false, label:"TestFindModified2", parentForm:"5ae0bc70906f292440482bae", state:"default", textAide:"Info", type:"Date", value:"-" }})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('nModified').eq(1);
                  done();
                });
          });
      });
   });

   /*
  * Test the /PUT route
  */
   describe('/PUT update task', () => {
      it('it should UPDATE a task given the id type LIST', (done) => {
        let task = new TaskModel( { childDataSet:{
              dataset: [{id: 1524680126541, title: "Pending", status: "warning"},
                        {id: 1524680126542, title: "Refused", status: "error"},
                        {id: 1524680126543, title: "Accepted", status: "success"},
                        {id: 1524680126544, title: "To start", status: "default"}]},
            defaultValueID:1524680126544,
            isPartOfOneTemplate:false,
            label:"Dataset",
            parentForm:"5ae0bc70906f292440482bae",
            state:"default",
            textAide:"MyInfo",
            type:"List"
          }
        );
        task.save((err, task) => {
                chai.request(server)
                .put('/api-dev/tasks/')
                .send({ "task" : {_id: task._id,
                                  childDataSet:{
                                    dataset: [{id: 1524680126541, title: "Pending2", status: "warning"},
                                            {id: 1524680126542, title: "Refused2", status: "error"},
                                            {id: 1524680126543, title: "Accepted2", status: "success"},
                                            {id: 1524680126544, title: "To start2", status: "default"}]
                                    },
                                  defaultValueID:1524680126544,
                                  isPartOfOneTemplate:false,
                                  label:"DatasetModified",
                                  parentForm:"5ae0bc70906f292440482bae",
                                  state:"default",
                                  textAide:"MyInfo",
                                  type:"List" }})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('nModified').eq(1);
                  done();
                });
          });
      });
   });

   /*
  * Test the /PUT route
  */
   describe('/PUT update type task', () => {
      it('it should UPDATE a task type STRING to DATE', (done) => {
        let task = new TaskModel({ isPartOfOneTemplate:false, label:"STRING", parentForm:"5ae0bc70906f292440482bae", state:"default", textAide:"Info", type:"String", value:"-" });
        task.save((err, task) => {
                chai.request(server)
                .put('/api-dev/tasks/')
                .send({ "task" : {_id: task._id, isPartOfOneTemplate:false, label:"STRING to DATE", parentForm:"5ae0bc70906f292440482bae", state:"default", textAide:"Info", type:"Date", value:"-" }})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('nModified').eq(1);
                  done();
                });
          });
      });
   });

   /*
  * Test the /PUT route
  */
   describe('/PUT update type task', () => {
      it('it should UPDATE a task type DATE to STRING', (done) => {
        let task = new TaskModel({ isPartOfOneTemplate:false, label:"DATE", parentForm:"5ae0bc70906f292440482bae", state:"default", textAide:"Info", type:"Date", value:"-" });
        task.save((err, task) => {
                chai.request(server)
                .put('/api-dev/tasks/')
                .send({ "task" : {_id: task._id, isPartOfOneTemplate:false, label:"DATE to STRING", parentForm:"5ae0bc70906f292440482bae", state:"default", textAide:"Info", type:"String", value:"-" }})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('nModified').eq(1);
                  done();
                });
          });
      });
   });

   /*
  * Test the /DELETE/:id route
  */
  describe('/DELETE/:id task', () => {
      it('it should DELETE a task by the given id', (done) => {
        let task = new TaskModel({ isPartOfOneTemplate:false, label:"TestDelete", parentForm:"5ae0bc70906f292440482bae", state:"default", textAide:"Info", type:"String", value:"-" });
        task.save((err, myVar) => {
            chai.request(server)
            .delete('/api-dev/tasks/')
            .send({ "task" : task})
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.task.should.have.property('ok').eq(1);
              done();
            });
        });

      });
  });


});
