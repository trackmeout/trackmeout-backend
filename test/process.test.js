import mongoose from "mongoose" ;
import ProcessModel from '../models/ProcessModel';
import ModuleModel from '../models/ModuleModel';
import chai from 'chai';
import chaiHttp from 'chai-http';
import server from '../index.js';
let should = chai.should();

chai.use(chaiHttp);

describe('Process unit test', () => {

  let id = 0;
  let idTemplate = 0;
  /*
  * Test delete all processes
  */
  describe('Delete processes', () => {
      it('it should DELETE all processes', (done) => {
        ProcessModel.remove({}, (err) => {
          if(err === null)
            done(err);
          else
           done();
        });
      });
  });

  /*
  * Test the /POST route
  */
  describe('/POST new process ', () => {
      it('it should POST new process', (done) => {
        let myVar = {process: {name: "Test", owner: "5ae35f5ad843ac451404933f"}};
        chai.request(server)
            .post('/api-dev/processes/')
            .send(myVar)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.process.should.have.property('isArchived').eq(false);
                res.body.process.should.have.property('isPartOfOneTemplate').eq(false);
                res.body.process.should.have.property('_id');
                res.body.process.should.have.property('name').eq('Test');
                res.body.process.should.have.property('owner').eq('5ae35f5ad843ac451404933f');
                res.body.process.should.have.property('modules').eq(null);
                done();
            });
      });

  });

  /*
  * Test the /POST route
  */
  describe('/POST new process ', () => {
      it('it should POST new template process ', (done) => {
        let myVar = {process: {name: "TestTemplate", isTemplate:true}};
        chai.request(server)
            .post('/api-dev/processes/')
            .send(myVar)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.process.should.have.property('isArchived').eq(false);
                res.body.process.should.have.property('isPartOfOneTemplate').eq(false);
                res.body.process.should.have.property('isTemplate').eq(true);
                res.body.process.should.have.property('_id');
                res.body.process.should.have.property('name').eq('TestTemplate');
                res.body.process.should.have.property('modules').eq(null);
                done();
            });
      });

  });


  /*
  * Test the /GET route
  */
  describe('/GET processes', () => {
      it('it should GET all processes', (done) => {
        chai.request(server)
            .get('/api-dev/processes/')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.processes.length.should.be.eql(2);
                res.body.processes[0].should.have.property('isPartOfOneTemplate').eq(false);
                res.body.processes[0].should.have.property('_id');
                res.body.processes[0].should.have.property('isTemplate').eq(false);
                res.body.processes[0].should.have.property('name').eq('Test');
                res.body.processes[0].should.have.property('modules').eq(null);
                res.body.processes[0].should.have.property('owner').eq('5ae35f5ad843ac451404933f');
                id = res.body.processes[0]._id;
                done();
            });
      });
  });

  /*
  * Test the /GET route
  */
  describe('/GET processes', () => {
      it('it should GET all processes templates', (done) => {
        chai.request(server)
            .get('/api-dev/processes/are-templates')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.processes.length.should.be.eql(1);
                res.body.processes[0].should.have.property('isPartOfOneTemplate').eq(false);
                res.body.processes[0].should.have.property('_id');
                res.body.processes[0].should.have.property('isTemplate').eq(true);
                res.body.processes[0].should.have.property('name').eq('TestTemplate');
                res.body.processes[0].should.have.property('modules').eq(null);
                idTemplate = res.body.processes[0]._id;
                done();
            });
      });
  });

  /*
  * Test the /PUT route
  */
  describe('/PUT modify process ', () => {
      it('it should change name to process ', (done) => {
        let myVar = {process: {name: "TestTemplate2", "id" : id}};
        chai.request(server)
            .put('/api-dev/processes/')
            .send(myVar)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.should.have.property('nModified').eq(1);
                done();
            });
      });
  });

  /*
  * Test the /GET route
  */
  describe('/GET processes', () => {
      it('it should GET all processes by owner', (done) => {
        chai.request(server)
            .get('/api-dev/processes/owner/'+'5ae35f5ad843ac451404933f')
            .send({"params":{"id":"5ae35f5ad843ac451404933f"}})
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.processes.length.should.be.eql(1);
                res.body.processes[0].should.have.property('isPartOfOneTemplate').eq(false);
                res.body.processes[0].should.have.property('_id');
                res.body.processes[0].should.have.property('isTemplate').eq(false);
                res.body.processes[0].should.have.property('name').eq('TestTemplate2');
                res.body.processes[0].should.have.property('modules');
                idTemplate = res.body.processes[0]._id;
                done();
            });
      });
    });

    /*
    * Test the /POST route
    */
    describe('/POST  process ', () => {
        it('it should convert process to template  ', (done) => {
          let myVar = {process: {"id": id, name: "CovertedAsTemplate"}};
          chai.request(server)
              .post('/api-dev/processes/convert-as-template/')
              .send(myVar)
              .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.a('Object');
                  res.body.should.have.property('success').eq(true);
                  done();
              });
        });

    });

    /*
    * Test the /POST route
    */
    describe('/POST  process ', () => {
        it('it should add process from template  ', (done) => {
          let process = new ProcessModel({
              name:"newProcess"
          });
          process.save((err, process) => {
            chai.request(server)
                .post('/api-dev/processes/add-processes-from-template/')
                .send({processes : [process], patientID : "5ae35f5ad843ac451404933f"})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('Object');
                    res.body.should.have.property('success').eq(true);
                    done();
                });
            });
        });

    });


        /*
        * Test the /POST route
        */
        describe('/POST  process ', () => {
            it('it should add module template to process template  ', (done) => {
              let module = new ModuleModel({
                  name:"moduleTemplate",
                  isTemplate : true
              });
              module.save((err, mod) => {
                chai.request(server)
                    .post('/api-dev/processes/add-module-temp-to-proc-temp/')
                    .send({processes : idTemplate, data : mod})
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('Object');
                        res.body.should.have.property('success').eq(true);
                        done();
                    });
                });
            });

        });


    /*
    * Test the /PUT route
    */
    describe('/PUT modify process ', () => {
        it('it should change name to process ', (done) => {
          let myVar = {processId : id};
          chai.request(server)
              .put('/api-dev/processes/archive/')
              .send(myVar)
              .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.a('Object');
                  res.body.should.have.property('nModified').eq(1);
                  done();
              });
        });
    });
});
