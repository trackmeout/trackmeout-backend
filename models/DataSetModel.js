import mongoose from "mongoose";
var Schema = mongoose.Schema;

//dataset model
var datasetSchema = Schema
(
    {
        dataset:
        [{
          title: {
              type: String,
              required: true
          },
          _id : false ,
          id: {
              type: String,
              required: true
          },
          status: {
              type: String,
              enum: ['success', 'error', 'default', 'warning'],
              required: true
          }
        }],
    },
    {
        timestamps: true
    }
);


module.exports = mongoose.model('DataSet', datasetSchema);
