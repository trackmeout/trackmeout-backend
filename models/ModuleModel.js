import mongoose from "mongoose";
var Schema = mongoose.Schema;

//Module model
var moduleSchema = Schema
(
    {
        name: {
            type: String,
            required: true
        },
        roles: [{
            type: Schema.ObjectId,
            ref: 'Role'
        }],
        parentProcess: {
            type: Schema.ObjectId,
            ref: 'Processus'
        },
        isTemplate: {
            type: Boolean,
            required: true,
            default: false
        },
        isPartOfOneTemplate: {
            type: Boolean,
            required: true,
            default: false
        },
        parentTemplate: {
            type: Schema.ObjectId,
            ref: 'Module'
        },
    },
    {
        timestamps: true,
        toJSON: { virtuals: true }
    }
);
moduleSchema.virtual('forms', {
    ref: 'Form',
    localField: '_id',
    foreignField: 'parentModule',
    justOne: false
});

export default mongoose.model("Module", moduleSchema);
