import mongoose from "mongoose" ;
import FormModel from '../models/FormModel';
import chai from 'chai';
import chaiHttp from 'chai-http';
import server from '../index.js';
let should = chai.should();

chai.use(chaiHttp);

describe('Form unit test', () => {

  let id = 0;
  let idTemplate = 0;
  let parentModule = 0;
  /*
  * Test delete all forms
  */
  describe('Delete forms', () => {
      it('it should DELETE all forms', (done) => {
        FormModel.remove({}, (err) => {
          if(err === null)
            done(err);
          else
           done();
        });
      });
  });

  /*
  * Test the /POST route
  */
  describe('/POST new forms ', () => {
      it('it should POST new form ', (done) => {
        let myVar = {
          form : {
            name:"abc",
            parentModule:"5ae31dfee2b1fc26a8a18d2c"
          }
        }
        chai.request(server)
            .post('/api-dev/forms/')
            .send(myVar)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.form.should.have.property('isPartOfOneTemplate').eq(false);
                res.body.form.should.have.property('_id');
                res.body.form.should.have.property('isTemplate').eq(false);
                res.body.form.should.have.property('parentModule').eq('5ae31dfee2b1fc26a8a18d2c');
                res.body.form.should.have.property('name').eq('abc');
                res.body.form.should.have.property('tasks').eq(null);
                parentModule = res.body.form.parentModule;
                id = res.body.form._id;
                done();
            });
      });

  });

  /*
  * Test the /GET route
  */
  describe('/GET forms', () => {
      it('it should GET all forms', (done) => {
        chai.request(server)
            .get('/api-dev/forms/')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.forms.length.should.be.eql(1);
                res.body.forms[0].should.have.property('isPartOfOneTemplate').eq(false);
                res.body.forms[0].should.have.property('_id');
                res.body.forms[0].should.have.property('isTemplate').eq(false);
                res.body.forms[0].should.have.property('parentModule').eq('5ae31dfee2b1fc26a8a18d2c');
                res.body.forms[0].should.have.property('name').eq('abc');
                res.body.forms[0].should.have.property('tasks').eq(null);
                done();
            });
      });
  });

  /*
  * Test the /GET route
  */
  describe('/GET forms by id', () => {
      it('it should GET form by id', (done) => {
        chai.request(server)
            .get('/api-dev/forms/owner/'+parentModule)
            .send({"params" : parentModule})
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.forms.length.should.be.eql(1);
                res.body.forms[0].should.have.property('isPartOfOneTemplate').eq(false);
                res.body.forms[0].should.have.property('_id');
                res.body.forms[0].should.have.property('isTemplate').eq(false);
                res.body.forms[0].should.have.property('parentModule').eq('5ae31dfee2b1fc26a8a18d2c');
                res.body.forms[0].should.have.property('name').eq('abc');
                done();
            });
      });
  });

  /*
  * Test the /PUT route
  */
  describe('/PUT forms by id', () => {
      it('it should modify form by id', (done) => {
        let myVar = {
          form : {
            "name":"abc",
            "parentModule":"5ae31dfee2b1fc26a8a18d2c",
            "id":id
          }
        }
        chai.request(server)
            .put('/api-dev/forms/')
            .send(myVar)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.should.have.property('nModified').eq(1);
                done();
            });
      });
  });

  /*
  * Test the /POST route
  */
  describe('/POST new forms as template', () => {
      it('it should POST new form ', (done) => {
        let myVar = {
          form : {
            name:"abcTemplate",
            parentModule:"5ae31dfee2b1fc26a8a18d2c",
            isTemplate:true,
          }
        }
        chai.request(server)
            .post('/api-dev/forms/')
            .send(myVar)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.form.should.have.property('isPartOfOneTemplate').eq(false);
                res.body.form.should.have.property('_id');
                res.body.form.should.have.property('isTemplate').eq(true);
                res.body.form.should.have.property('parentModule').eq('5ae31dfee2b1fc26a8a18d2c');
                res.body.form.should.have.property('name').eq('abcTemplate');
                res.body.form.should.have.property('tasks').eq(null);
                parentModule = res.body.form.parentModule;
                id = res.body.form._id;
                done();
            });
      });

  });

  /*
  * Test the /GET route
  */
  describe('/GET forms as template', () => {
      it('it should GET form as template', (done) => {
        chai.request(server)
            .get('/api-dev/forms/templates/')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.forms.length.should.be.eql(1);
                res.body.forms[0].should.have.property('isPartOfOneTemplate').eq(false);
                res.body.forms[0].should.have.property('_id');
                res.body.forms[0].should.have.property('isTemplate').eq(true);
                res.body.forms[0].should.have.property('parentModule').eq('5ae31dfee2b1fc26a8a18d2c');
                res.body.forms[0].should.have.property('name').eq('abcTemplate');
                idTemplate = res.body.forms[0]._id;
                done();
            });
      });
  });

  /*
  * Test the /GET route
  */
  describe('/GET forms as template by id', () => {
      it('it should GET form by id', (done) => {
        chai.request(server)
            .get('/api-dev/forms/templates/'+idTemplate)
            .send({"params" : idTemplate})
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.forms.should.have.property('isPartOfOneTemplate').eq(false);
                res.body.forms.should.have.property('_id');
                res.body.forms.should.have.property('isTemplate').eq(true);
                res.body.forms.should.have.property('parentModule').eq('5ae31dfee2b1fc26a8a18d2c');
                res.body.forms.should.have.property('name').eq('abcTemplate');
                done();
            });
      });
  });

  /*
  * Test the /POST route
  */
  describe('/POST convert form  ', () => {
      it('it should convert form as template ', (done) => {
        let myVar = {
          form : {
            "id": id,
            name:"abcConvertedAsTemplate",
            tasks: [
              {
                documents: [],
                isPartOfOneTemplate: false,
                _id: "5ac7e5fb998aad162835711f",
                label: "Main Courante",
                type: "String",
                parentForm: id,
                state: "default",
                textAide: "",
                value: "-",
                childDataSet: null
              }]
          }
        }
        chai.request(server)
            .post('/api-dev/forms/convert-as-template/')
            .send(myVar)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.should.have.property('success').eq(true);
                done();
            });
      });
  });

  /*
 * Test the /DELETE/:id route
 */
 describe('/DELETE/:id form', () => {
     it('it should DELETE a form by the given id', (done) => {
         chai.request(server)
         .delete('/api-dev/forms/')
         .send({ "form" : {_id : idTemplate}})
         .end((err, res) => {
             res.should.have.status(200);
             res.body.should.be.a('object');
             res.body.form.should.have.property('ok').eq(1);
           done();
         });
     });
 });


});
