import mongoose from "mongoose" ;
import PatientModel from '../models/PatientModel';
import chai from 'chai';
import chaiHttp from 'chai-http';
import server from '../index.js';
let should = chai.should();

chai.use(chaiHttp);

describe('Patients unit test', () => {

  /*
  * Test delete all patients
  */
  describe('Delete patients', () => {
      it('it should DELETE all patients', (done) => {
        PatientModel.remove({}, (err) => {
          if(err === null)
            done(err);
          else
           done();
        });
      });
  });

  /*
  * Test the /POST route
  */
  describe('/POST new patients', () => {
      it('it should POST new patients', (done) => {
        let myVar = {
          patient : {
            birthDate:"02.04.2018",
            firstName:"Unit",
            gender:"0",
            lastName:"Test"
          }
        }
        chai.request(server)
            .post('/api-dev/patients/')
            .send(myVar)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.patient.should.have.property('_id');
                res.body.patient.should.have.property('birthDate').eq("02.04.2018");
                res.body.patient.should.have.property('gender').eq(0);
                res.body.patient.should.have.property('lastName').eq("Test");
                res.body.patient.should.have.property('createdAt');
                res.body.patient.should.have.property('updatedAt');
                done();
            });
      });

  });

  /*
  * Test the /GET route
  */
  describe('/GET patients', () => {
      it('it should GET all patients', (done) => {
        chai.request(server)
            .get('/api-dev/patients/')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                done();
            });
      });
  });
});
