import path from 'path'
import webpack from 'webpack';

export default {
    devtools: 'eval-source-map',
    entry: [
        'webpack-hot-middleware/client',
        path.join(__dirname, '/index.js')
    ],
    output: {
        path: '/',
        publicPath: '/'
    },
    plugins: [
      new webpack.NoErrorsPlugin(),
      new webpack.optimize.OccurrenceOrderPlugin(),
      new webpack.HotModuleReplacementPlugin()
    ],
    module: {
        loaders: [
            {
                test: /\.js$/,
                include: [
                    ],
                loaders: [ 'react-hot', 'babel' ]
            }
        ]
    },
    resolve: {
        extentions: [ '', '.js' ]
    },
    node: {
        net: "empty",
        dns: "empty"
    }
}
