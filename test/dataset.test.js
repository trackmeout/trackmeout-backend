import mongoose from "mongoose" ;
import DatasetModel from '../models/DataSetModel';
import chai from 'chai';
import chaiHttp from 'chai-http';
import server from '../index.js';
let should = chai.should();

chai.use(chaiHttp);

describe('Datasets unit test', () => {
  /*
  * Test delete all forms
  */
  describe('Delete all datasets', () => {
      it('it should DELETE all datasets', (done) => {
       DatasetModel.remove({ _id : { $ne: "5ad9faa5c9eb393644d4203c" } }, (err) => {
          if(err === null)
            done(err);
          else
           done();
        });
      });
  });
  /*
  * Test the /GET route
  */
  describe('/GET datasets', () => {
      it('it should GET all datasets', (done) => {
        chai.request(server)
            .get('/api-dev/datasets/')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.dataSet.should.be.a('array');
                res.body.dataSet[0].dataset[0].should.have.property('id');
                res.body.dataSet[0].dataset[0].should.have.property('title');
                res.body.dataSet[0].dataset[0].should.have.property('status');
                done();
            });
      });
  });
});
