import express from "express";
import DataSetModel from '../models/DataSetModel';

const router = express.Router();


/**
 * Getting every DataSet
 *
 * @param {type} req Description
 * @param {type} res Description
 *
 * @return {type} Description
 */
router.get("/", (req, res) => {
    DataSetModel.find({}).then(dataSet => res.json({dataSet}));
});

export default router;
