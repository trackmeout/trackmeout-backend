import express from "express";
import PatientModel from "../models/PatientModel";
import ProcessModel from "../models/ProcessModel";
import { countProcessByPatient } from '../utils/global';
const router = express.Router();

/**
* get all patients with their processus, modules forms, tasks... etc
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.get("/", (req, res) => {
  PatientModel.find({}).then(patients =>
    {
    if(patients.length > 0)
    {
      var responsePatients = [];
      var elementNumber = 0;
      patients.map(function(currentPatient){

        ProcessModel
            .find({"owner": currentPatient._id, isArchived: false})
            .populate({
                path: 'modules',
                populate: {
                    path: 'forms',
                    populate: {
                        path: 'tasks',
                        populate: {
                          path: 'childDataSet'
                        }
                    }
                }
            })
            .exec(function (error, processes) {
                var moduleData = [];
                var formData = [];
                const processesData = JSON.parse(JSON.stringify(processes));

                  let totalProcesses = processesData.length;
                  let totalModules = 0;
                  let totalForms = 0;
                  let totalTasks = 0;

                  let totalProcessesDone = 0;
                  let totalModulesDone = 0;
                  let totalFormsDone = 0;
                  let totalTasksDone = 0;


                  //Module
                  var currentModuleFormsLength;

                  //forms
                  var currentFormTasksLenght;

                  //Task
                  var cptTask = 0;
                  var isAllTasksDone = true;

                  processesData.map(function(process){
                    totalProcesses++;
                    var currentProcessModules = process.modules.length;

                    process.modules.map(function(module){
                      totalModules++;
                      currentModuleFormsLength = module.forms.length;

                      module.forms.map(function(form){
                        totalForms++;
                        currentFormTasksLenght = form.tasks.length;

                        form.tasks.map(function(task){
                          cptTask++;
                          totalTasks++;

                          if (task.state == "success"){
                            totalTasksDone ++;
                          }else{
                            isAllTasksDone = false;
                          }

                          //End of loop
                          if (cptTask == currentFormTasksLenght){
                              if(isAllTasksDone == true){
                                totalFormsDone++;
                                if(totalFormsDone == currentModuleFormsLength){
                                  totalModulesDone++;
                                }
                              }
                            //Reset values for calcul
                            isAllTasksDone = true;
                            cptTask = 0;
                          }

                        });
                      });
                    });
                  });

                  var currentPatientModified = JSON.parse(JSON.stringify(currentPatient));
                  currentPatientModified.totalModules = totalModules
                  currentPatientModified.totalForms = totalForms
                  currentPatientModified.totalTasks = totalTasks

                  currentPatientModified.totalModulesDone = totalModulesDone
                  currentPatientModified.totalFormsDone = totalFormsDone
                  currentPatientModified.totalTasksDone = totalTasksDone

                  responsePatients.push( currentPatientModified )


                  elementNumber++;
                  if(elementNumber === patients.length){
                      res.status(200).json({ patients: responsePatients });
                  }
                });
              })
          }
          else{
            res.status(200).json({ patients: [] });
          }
      });
  });

  /**
  * create a patients
  *
  * @param {type} req Description
  * @param {type} res Description
  *
  * @return {type} Description
  */
router.post("/", (req, res) => {
  PatientModel.create(req.body.patient)
    .then(patient => res.json({ patient }))
    .catch(err => res.status(400).json({ errors: parseErrors(err.errors) }));
});


export default router;
