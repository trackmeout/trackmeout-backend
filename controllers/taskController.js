import express from "express";
import TaskModel from '../models/TaskModel';
import DataSetModel from '../models/DataSetModel';
import { deleteTask } from '../utils/global';
import _ from 'lodash';
const router = express.Router();

/**
* get all tasks
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.get("/", (req, res) => {
    TaskModel.find({}).then(task => res.json({task}));
});

/**
* get tasks by id
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.get("/:id", (req, res) => {
    var id = req.params.id;
    TaskModel.find({"_id":id}).then(task => res.json({task}));
});

//DELETE
router.delete("/", (req, res) =>{
  var idDelete = req.body.task;
  deleteTask(idDelete).then(task => res.json({task}));
});

//MODIFICATION
router.put("/", (req, res) => {
  if( req.body.task.childDataSet !== null &&  !_.isEmpty(req.body.task.childDataSet ) ){
    //DASASET TRUE
    if( req.body.task.currentDataSetId !== undefined){
      //Mise à jour List
      var queryDataSet = { _id : req.body.task.currentDataSetId};
      DataSetModel.update(queryDataSet, req.body.task.childDataSet)
      .then( () => {
        var queryTask = { _id : req.body.task._id };
        TaskModel.update(queryTask, req.body.task, function(err, raw) {
          if (err) {
            res.send(err);
          }
          res.send(raw);
        });
      });
    }
    else{
      //Creation de standard -> List
      DataSetModel.create(req.body.task.childDataSet)
        .then( (data) => {
          req.body.task['currentDataSetId']=data._id;
          var queryTask = { _id : req.body.task._id };
          TaskModel.update(queryTask, req.body.task, function(err, raw) {
            if (err) {
              res.send(err);
            }
            res.send(raw);
          });
      });
    }
  }
  else{
    //PAS DE DATASET
    var queryTask = { _id : req.body.task._id };
    TaskModel.update(queryTask, req.body.task, function(err, raw) {
      if (err) {
        res.send(err);
      }
      res.send(raw);
    });
  }
});

//CREATION
router.post("/", (req, res) => {
  if(req.body.task.childDataSet !== null && req.body.task.childDataSet !== undefined){
    DataSetModel.create(req.body.task.childDataSet)
      .then(function(data) {
        req.body.task['currentDataSetId']=data._id;
        TaskModel.create(req.body.task)
         .then(task => res.json({ task }));
      });
  }
  else{
    TaskModel.create(req.body.task)
       .then(task => res.json({ task }));
  }
});

export default router;
