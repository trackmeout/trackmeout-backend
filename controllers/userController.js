import express from "express";
import UserModel from "../models/UserModel";
import bcrypt from "bcrypt";

const router = express.Router();

//CREATION
router.post("/login", (req, res) => {
  const { credentials } = req.body;
  UserModel.findOne({ email: credentials.email }).then(user => {
    if (user && user.isValidPassword(credentials.password)) {
      res.json({ user: user.toAuthJSON() });
    } else {
      res.json({ errors: { global: "Invalid credentials" } });
    }
  });
});

/**
* create new user
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.post("/signup", (req, res) => {
  const { email, password, lastName, firstName } = req.body.user
  const user = new UserModel({ email, lastName, firstName });
  user.setPassword(password);
  user
    .save()
    .then(userRecord => {
      res.json({ user: userRecord.toAuthJSON() });
    })
    .catch(err => res.json({ err }));
});

export default router;
