import mongoose from "mongoose";
var Schema = mongoose.Schema;

//Patient Model
var processSchema = Schema
(
    {
        name: {
            type: String,
            required: true
        },
        isArchived: {
            type: Boolean,
            required: true,
            default: false
        },
        roles: [{
            type: Schema.ObjectId,
            ref: 'Role'
        }],
        owner: {
            type: mongoose.Schema.Types.ObjectId,
            ref:"Patient"
        },
        isTemplate: {
            type: Boolean,
            required: true,
            default: false
        },
        isPartOfOneTemplate: {
            type: Boolean,
            required: true,
            default: false
        },
        parentTemplate: {
            type: Schema.ObjectId,
            ref: 'Processus'
        },
    },
    {
        timestamps: true,
        toJSON: { virtuals: true }
    }
);
processSchema.virtual('modules', {
    ref: 'Module',
    localField: '_id',
    foreignField: 'parentProcess',
    justOne: false
});


export default mongoose.model("Processus", processSchema);
