import express from "express";
import FormModel from '../models/FormModel';
import TaskModel from '../models/TaskModel';
import { deleteForm } from '../utils/global';
import ModuleModel from "../models/ModuleModel";
const router = express.Router();


/**
 * get of all formular
 *
 * @param {type} req Description
 * @param {type} res Description
 *
 * @return {type} Description
 */
router.get("/", (req, res) => {
  FormModel.find({ }).then(forms => res.json({ forms }))
});

/**
* get all form from a parent modules
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.get("/owner/:id", (req, res) => {
  var id = req.params.id;
  FormModel.find({parentModule: id})
  .populate({
          path: 'tasks',
          populate: {
              path: 'childDataSet'
          }
        })
  .exec(function (error, forms) {
          res.json({forms});
      }
  );
});


/**
* update one form by his ID
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/

/**
* Delete a form
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.delete("/", (req, res) =>{
  var formId = req.body.form._id;
  deleteForm(formId).then(form => res.json({form}));
});

/**
* create / add a new form
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.post("/", (req, res) => {
  FormModel.create(req.body.form)
    .then(form => res.json({ form }))
    .catch(err => res.status(400).json({ errors: parseErrors(err.errors) }));

});

/**
* update a form name
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.put("/", (req, res) => {
  var query = { _id : req.body.form.id };
  FormModel.update(query, { name : req.body.form.name }, function(err, raw) {
    if (err) {
      res.send(err);
    }
    res.send(raw);
  });
});

/**
* get all templates form
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.get("/templates", (req, res) => {
    FormModel.find({isTemplate: true})
    .populate({
            path: 'tasks',
            populate: {
                path: 'childDataSet'
            }
          })
    .exec(function (error, forms) {
            res.json({forms});
        }
    );
});

/**
* converting a simple form as a template form
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.post("/convert-as-template", (req, res) => {
    const formId = req.body.form.id;
    const newFormName = req.body.form.name;
    var totalNbrElementsToAdd = 0;
    var totalNbrElementsAdded = 0;

    function sendResponse() {
        if(totalNbrElementsToAdd == totalNbrElementsAdded){
            res.json({success: totalNbrElementsToAdd > 0 });
        }
    }

    FormModel
        .findOne({_id: formId})
        .populate({
                path: 'tasks',
                populate: {
                    path: 'childDataSet'
                }
        })
        .exec(function (error, oldForm) {
                totalNbrElementsToAdd++;
                    const newForm =  new FormModel({
                        name: newFormName,
                        parentModule: null,
                        isTemplate: true,
                        isPartOfOneTemplate: false,
                        parentTemplate: null,
                        isArchived: false,
                        parentModule:null,
                    });
                    newForm.save((err, formSaved) => {
                        if (err) {
                            res.json({success: false});
                            return;
                        }
                        totalNbrElementsAdded++;
                        oldForm.tasks.map(( task ) => {
                            totalNbrElementsToAdd++;
                            let NewTask;
                            if(task.type === "List"){
                                NewTask =  new TaskModel({
                                    label: task.label,
                                    value: '-',
                                    type: task.type,
                                    isTemplate: false,
                                    isPartOfOneTemplate: task.isTemplate === true,
                                    textAide: task.textAide,
                                    documents:[],
                                    defaultValueID: task.defaultValueID,
                                    currentDataSetId: task.currentDataSetId,
                                    parentForm: formSaved._id,
                                });
                            }
                            else{
                                NewTask =  new TaskModel({
                                    label: task.label,
                                    value: '-',
                                    type: task.type,
                                    isTemplate: false,
                                    isPartOfOneTemplate: task.isTemplate === true,
                                    textAide: task.textAide,
                                    documents:[],
                                    parentForm: formSaved._id,
                                    state: 'default'
                                });
                            }

                            NewTask.save((err, taskSaved) => {
                                if (err) {
                                    res.json({success: false});
                                    return;
                                }
                                else{
                                    totalNbrElementsAdded++;
                                    sendResponse();
                                }
                            });
                            sendResponse();
                        });
                        sendResponse();
            });
        });
});

/**
* Getting form templates by ID
*
* @param {type} req Description
* @param {type} res Description
*
* @return {type} Description
*/
router.get("/templates/:id", (req, res) => {
    var id = req.params.id;
    FormModel.findOne({_id: id})
        .populate({
                path: 'tasks',
                populate: {
                    path: 'childDataSet'
                }
              })
        .exec(function (error, forms) {
                res.json({forms});
            }
        );
});
export default router;
