import express from "express";
import fileUpload from 'express-fileupload';
import uuidv1 from "uuid/v1";
import fs from "fs-extra"
const router = express.Router();

// default options
router.use(fileUpload());

//Create
router.post("/", (req, res) => {
    if (!req.files)
      return res.status(400).send('No files were uploaded.');
    let error = 0;
    let files = req.files['files[]'];
    let filesName = [];
    if(files.length !== undefined){
      for (var i = 0; i < files.length; i++) {
        let fileName = uuidv1()+'@'+files[i].name;
        filesName.push(fileName)
        files[i].mv('./uploads/'+fileName, function(err) {
            if(err)
              error++;
        });
      }
    }
    else{
      let fileName = uuidv1()+'@'+files.name;
      filesName.push(fileName)
      files.mv('./uploads/'+fileName, function(err) {
          if(err)
            error++;
      });
    }
    if (error>0)
      return res.status(500).send(err);
    res.send({'UPLOAD_OK' : filesName});
});

//DELETE
router.delete("/", (req, res) =>{
  var id = req.body.document;
  fs.remove('./uploads/'+id, err => {
    if (err)
      return console.error(err)
    else
      res.send({'DELETE_OK' : id});

  })
});

//Get document pdf
router.get("/:id", (req, res) => {
    var id = req.params.id;
    var fileName = './uploads/'+id;
    fs.readFile(fileName , function (err,data, id){
        res.header('Content-disposition', 'inline; filename=' + id);
        res.header('Content-type', 'application/pdf');
        res.send(data)
    });
});




export default router;
