/**
 * Created by Tiago on 05.02.2017.
 */
import express from "express";
import path from "path";
import bodyParser from "body-parser";
import webpack from "webpack";
import cors from "cors";
import webpackMiddleware from "webpack-dev-middleware";
import webpackHotMiddleware from "webpack-hot-middleware";
import webpackConfig from "./webpack.config.dev";
import mongoose from "mongoose";
import Promise from "bluebird";

try {
    var configFile = require('./config.js');
} catch (ex) {
    var configFile = require('./config.js.dist');
}
/* Import des controlleurs
 *************************************************************/
import processController from './controllers/processController';
import patientController from './controllers/patientController';
import taskController from './controllers/taskController';
import formController from './controllers/formController';
import moduleController from './controllers/moduleController';
import datasetController from './controllers/datasetController';
import userController from './controllers/userController';
import documentsController from './controllers/documentsController';

const config = configFile.config;
const ENV_PRODUCTION = configFile.ENV_PRODUCTION;

/* Lancement de l'application
 *************************************************************/
const app = express();
app.use(cors());
app.use(bodyParser.json());
/* Lancement de mongo
 *************************************************************/
mongoose.Promise = Promise;
mongoose.connect(config.mongoConfig.host);

/* Lancement de webpack
 *************************************************************/
const compiler = webpack(webpackConfig);
app.use(webpackMiddleware(compiler, {
    hot: true,
    publicPath: webpackConfig.output.publicPath,
    noInfo: true
}));
app.use(webpackHotMiddleware(compiler));

let apiPath = "api";
if(config.currentEnvironment !== ENV_PRODUCTION){
    apiPath = "api-dev";
}
/* Chargement des controlleurs / routes
 *************************************************************/
app.use("/"+apiPath+"/processes", processController);
app.use("/"+apiPath+"/patients", patientController);
app.use("/"+apiPath+"/forms", formController);
app.use("/"+apiPath+"/tasks", taskController);
app.use("/"+apiPath+"/modules", moduleController);
app.use("/"+apiPath+"/datasets", datasetController);
app.use("/"+apiPath+"/users", userController);
app.use("/"+apiPath+"/documents", documentsController);

//ne pas mettre de use après le get
app.get("/*",(req,res)=>{
    res.json({
        success: false,
        errors: [
            "invalid address"
        ]
    })
});


/* Lancement du serveur sur le port 5000
 *************************************************************/
module.exports = app.listen(config.port,"127.0.0.1",function(){
    console.log("Listening on port " + config.port)
});
