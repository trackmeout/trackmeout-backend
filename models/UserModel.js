import mongoose from "mongoose";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import uniqueValidator from "mongoose-unique-validator";

try {
    var configFile = require('./../config.js');
} catch (ex) {
    var configFile = require('./../config.js.dist');
}
const config = configFile.config;
var Schema = mongoose.Schema;

//User model
var userSchema = Schema
(
    {
        email: {
            type: String,
            unique: true,
            required: true,
            lowercase: true,
            index: true,
        },
        firstName: {
            type: String,
            required: true
        },
        lastName: {
            type: String,
            required: true
        },
        password: {
            type: String,
            required: true
        },
    }
    ,
    {
        timestamps: true
    }
);

userSchema.methods.isValidPassword = function isValidPassword(password) {
  return bcrypt.compareSync(password, this.password);
};

userSchema.methods.setPassword = function setPassword(password) {
  this.password = bcrypt.hashSync(password, 10);
};

userSchema.methods.generateJWT = function generateJWT() {
  return jwt.sign(
    {
      email: this.email,
      firstname : this.firstName,
      lastname : this.lastName,
    },
    config.jwt_secret
  );
};

userSchema.methods.toAuthJSON = function toAuthJSON() {
  return {
    email: this.email,
    firstname : this.firstName,
    lastname : this.lastName,
    token: this.generateJWT(),
  };
};

userSchema.plugin(uniqueValidator, { message: "This email is already taken" });

export default mongoose.model("User", userSchema);
