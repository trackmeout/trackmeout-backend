import mongoose from "mongoose" ;
import chai from 'chai';
import chaiHttp from 'chai-http';
import server from '../index.js';
let should = chai.should();

chai.use(chaiHttp);



describe('Documents unit test', () => {
  let id = 0;
  /*
  * Test the /POST route
  */
  describe('/POST new document', () => {
      it('it should POST new document', (done) => {

        chai.request(server)
            .post('/api-dev/documents/')
            .field('Content-Disposition', 'form-data')
            .field('name','files[]')
            .field('filename','tutorial-swig.pdf')
            .attach('files[]', './test/data/tutorial-swig.pdf')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.should.have.property('UPLOAD_OK');
                id = res.body.UPLOAD_OK[0];
                done();
            });
      });

  });

  /*
  * Test the /GET route
  */
  describe('/GET document', () => {
      it('it should GET all document', (done) => {
        chai.request(server)
            .get('/api-dev/documents/'+id)
            .send({"params":{"id":id}})
            .end((err, res) => {
                res.should.have.status(200);
                res.header.should.be.a('Object');
                res.header.should.have.property('x-powered-by').eq('Express');
                res.header.should.have.property('content-length').eq('91838');
                res.header.should.have.property('content-type').eq('application/pdf');
                res.header.should.have.property('connection').eq('close');
                done();
            });
      });
  });

  /*
  * Test the /DELETE route
  */
  describe('/DELETE document', () => {
      it('it should DELETE document by id', (done) => {
        chai.request(server)
            .delete('/api-dev/documents/')
            .send({"document":id})
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                res.body.should.have.property('DELETE_OK').eq(id);
                done();
            });
      });
  });
});
